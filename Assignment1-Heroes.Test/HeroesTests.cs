using Assignment1_Heroes.Exceptions;
using Assignment1_Heroes.Heroes;
using Assignment1_Heroes.Items;
using System.Globalization;

namespace Assignment1_Heroes.Test
{
    public class HeroesTests
    {
        //Create hero
        [Fact]
        public void Hero_CreateHeroWithName_ShouldReturnCorrectName()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);
            string expected = name;

            //Act
            string actual = heroMage.Name;  

            //Assert  
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_CreateHeroWithName_ShouldReturnCorrectLevel()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);
            int expected = 1;

            //Act
            int actual = heroMage.Level;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_CreateHeroWithName_ShouldReturnCorrectAttributeStrength()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);
            int expected = 1;

          
            //Act
            int actual = heroMage.LevelAttributes.Strength;

            //Assert
            Assert.Equal(expected, actual);
            
        }

        [Fact]
        public void Hero_CreateHeroWithName_ShouldReturnCorrectAttributeDexterity()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);
            int expected = 1;


            //Act
            int actual = heroMage.LevelAttributes.Dexterity;

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Hero_CreateHeroWithName_ShouldReturnCorrectAttributeIntelligence()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);
            int expected = 8;


            //Act
            int actual = heroMage.LevelAttributes.Intelligence;

            //Assert
            Assert.Equal(expected, actual);

        }

        //LevelUp
        [Fact]
        public void LevelIsIncreased_Mage_ShouldIncrementByCorrectAmount()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);
            int expected = 2;
            heroMage.LevelUp();

            //Act
            int actual = heroMage.Level;

            //Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void LevelIsIncreased_Ranger_ShouldIncrementByCorrectAmount()
        {
            //Arrange
            string name = "Thea";
            Hero heroRanger = new Ranger(name);
            int expected = 2;
            heroRanger.LevelUp();

            //Act
            int actual = heroRanger.Level;

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void LevelIsIncreased_Rogue_ShouldIncrementByCorrectAmount()
        {
            //Arrange
            string name = "Thea";
            Hero heroRogue = new Rogue(name);
            int expected = 2;
            heroRogue.LevelUp();

            //Act
            int actual = heroRogue.Level;

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void LevelIsIncreased_Warrior_ShouldIncrementByCorrectAmount()
        {
            //Arrange
            string name = "Thea";
            Hero heroWarrior = new Warrior(name);
            int expected = 2;
            heroWarrior.LevelUp();

            //Act
            int actual = heroWarrior.Level;

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void LevelIsIncreased_Mage_ShouldReturnCorrectAttributeStrength()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);
            heroMage.LevelUp();
            int expected = 2;

            //Act
            int actual = heroMage.LevelAttributes.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Mage_ShouldReturnCorrectAttributeDexterity()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);
            heroMage.LevelUp();
            int expected = 2;

            //Act
            int actual = heroMage.LevelAttributes.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Mage_ShouldReturnCorrectAttributeIntelligence()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);
            heroMage.LevelUp();
            int expected = 13;

            //Act
            int actual = heroMage.LevelAttributes.Intelligence;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Ranger_ShouldReturnCorrectAttributeStrength()
        {
            //Arrange
            string name = "Thea";
            Hero heroRanger = new Ranger(name);
            heroRanger.LevelUp();
            int expected = 2;

            //Act
            int actual = heroRanger.LevelAttributes.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Ranger_ShouldReturnCorrectAttributeDexterity()
        {
            //Arrange
            string name = "Thea";
            Hero heroRanger = new Ranger(name);
            heroRanger.LevelUp();
            int expected = 12;

            //Act
            int actual = heroRanger.LevelAttributes.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Ranger_ShouldReturnCorrectAttributeIntelligence()
        {
            //Arrange
            string name = "Thea";
            Hero heroRanger = new Ranger(name);
            heroRanger.LevelUp();
            int expected = 2;

            //Act
            int actual = heroRanger.LevelAttributes.Intelligence;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Rogue_ShouldReturnCorrectAttributeStrength()
        {
            //Arrange
            string name = "Thea";
            Hero heroRogue = new Rogue(name);
            heroRogue.LevelUp();
            int expected = 3;

            //Act
            int actual = heroRogue.LevelAttributes.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Rogue_ShouldReturnCorrectAttributeDexterity()
        {
            //Arrange
            string name = "Thea";
            Hero heroRogue = new Rogue(name);
            heroRogue.LevelUp();
            int expected = 10;

            //Act
            int actual = heroRogue.LevelAttributes.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Rogue_ShouldReturnCorrectAttributeIntelligence()
        {
            //Arrange
            string name = "Thea";
            Hero heroRogue = new Rogue(name);
            heroRogue.LevelUp();
            int expected = 2;

            //Act
            int actual = heroRogue.LevelAttributes.Intelligence;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Warrior_ShouldReturnCorrectAttributeStrength()
        {
            //Arrange
            string name = "Thea";
            Hero heroWarrior = new Warrior(name);
            heroWarrior.LevelUp();
            int expected = 8;

            //Act
            int actual = heroWarrior.LevelAttributes.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Warrior_ShouldReturnCorrectAttributeDexterity()
        {
            //Arrange
            string name = "Thea";
            Hero heroWarrior = new Warrior(name);
            heroWarrior.LevelUp();
            int expected = 4;

            //Act
            int actual = heroWarrior.LevelAttributes.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelIsIncreased_Warrior_ShouldReturnCorrectAttributeIntelligence()
        {
            //Arrange
            string name = "Thea";
            Hero heroWarrior = new Warrior(name);
            heroWarrior.LevelUp();
            int expected = 2;

            //Act
            int actual = heroWarrior.LevelAttributes.Intelligence;

            //Assert
            Assert.Equal(expected, actual);
        }

        //Create Weapon, correct name, required level, slot, weapon type, damage
        [Fact]
        public void Weapon_Create_ShouldReturnCorrectName()
        {
            //Arrange
            string weaponName = "staff";
            int requiredLevel = 2;
            WeaponType type = WeaponType.Staffs;
            double damage = 4;
            Slot slot = Slot.Weapon;

            Weapon newWeapon = new Weapon(weaponName, requiredLevel, type, damage, slot);

            string expected = weaponName;
            //Act
            string actual = newWeapon.Name;

            //Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void Weapon_Create_ShouldReturnCorrectRequiredLevel()
        {
            //Arrange
            string weaponName = "staff";
            int requiredLevel = 2;
            WeaponType type = WeaponType.Staffs;
            double damage = 4;
            Slot slot = Slot.Weapon;

            Weapon newWeapon = new Weapon(weaponName, requiredLevel, type, damage, slot);

            int expected = requiredLevel;

            //Act
            int actual = newWeapon.RequiredLevel;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Weapon_Create_ShouldReturnCorrecWeaponType()
        {
            //Arrange
            string weaponName = "staff";
            int requiredLevel = 2;
            WeaponType type = WeaponType.Staffs;
            double damage = 4;
            Slot slot = Slot.Weapon;

            Weapon newWeapon = new Weapon(weaponName, requiredLevel, type, damage, slot);

            WeaponType expected = type;

            //Act
            WeaponType actual = newWeapon.WeaponType;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Weapon_Create_ShouldReturnCorrectWeaponDamage()
        {
            //Arrange
            string weaponName = "staff";
            int requiredLevel = 2;
            WeaponType type = WeaponType.Staffs;
            double damage = 4;
            Slot slot = Slot.Weapon;

            Weapon newWeapon = new Weapon(weaponName, requiredLevel, type, damage, slot);

            double expected = damage;

            //Act
            double actual = newWeapon.WeaponDamage;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Weapon_Create_ShouldReturnWeaponSlot()
        {
            //Arrange
            string weaponName = "staff";
            int requiredLevel = 2;
            WeaponType type = WeaponType.Staffs;
            double damage = 4;
            Slot slot = Slot.Weapon;

            Weapon newWeapon = new Weapon(weaponName, requiredLevel, type, damage, slot);

            Slot expected = slot;

            //Act
            Slot actual = newWeapon.Slot;

            //Assert
            Assert.Equal(expected, actual);
        }

        //Create Armor, correct name, required level, slot, armor type, armor attributes

        [Fact]
        public void Armor_Create_ReturnCorrectName()
        {
            //Arrange
            string name = "shoes";
            int requiredLevel = 2;
            ArmorType type = ArmorType.Leather;
            Slot slot = Slot.Legs;
            HeroAttribute armorAttribute = new HeroAttribute(1, 3, 2);

            Armor newArmor = new Armor(name, requiredLevel, type, slot, armorAttribute);

            string expected = name;

            //Act
            string actual = newArmor.Name;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Armor_Create_ReturnCorrectRequiredLevel()
        {
            //Arrange
            string name = "shoes";
            int requiredLevel = 2;
            ArmorType type = ArmorType.Leather;
            Slot slot = Slot.Legs;
            HeroAttribute armorAttribute = new HeroAttribute(1, 3, 2);

            Armor newArmor = new Armor(name, requiredLevel, type, slot, armorAttribute);

            int expected = requiredLevel;

            //Act
            int actual = newArmor.RequiredLevel;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Armor_Create_ReturnCorrectArmorType()
        {
            //Arrange
            string name = "shoes";
            int requiredLevel = 2;
            ArmorType type = ArmorType.Leather;
            Slot slot = Slot.Legs;
            HeroAttribute armorAttribute = new HeroAttribute(1, 3, 2);

            Armor newArmor = new Armor(name, requiredLevel, type, slot, armorAttribute);

            ArmorType expected = type;

            //Act
            ArmorType actual = newArmor.ArmorType;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Armor_Create_ReturnCorrectSlot()
        {
            //Arrange
            string name = "shoes";
            int requiredLevel = 2;
            ArmorType type = ArmorType.Leather;
            Slot slot = Slot.Legs;
            HeroAttribute armorAttribute = new HeroAttribute(1, 3, 2);

            Armor newArmor = new Armor(name, requiredLevel, type, slot, armorAttribute);

            Slot expected = slot;

            //Act
            Slot actual = newArmor.Slot;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Armor_Create_ReturnCorrectArmorAttributes()
        {
            //Arrange
            string name = "shoes";
            int requiredLevel = 2;
            ArmorType type = ArmorType.Leather;
            Slot slot = Slot.Legs;
            HeroAttribute armorAttribute = new HeroAttribute(1, 3, 2);

            Armor newArmor = new Armor(name, requiredLevel, type, slot, armorAttribute);

            HeroAttribute expected = armorAttribute;

            //Act
            HeroAttribute actual = newArmor.ArmorAttribute;

            //Assert
            Assert.Equal(expected, actual);
        }


        // Equip Weapon
        [Fact]
        public void EquipWeapon_Staff_ShouldBeEquipped()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);
            string weaponName = "staff";
            int requiredLevel = 1;
            WeaponType type = WeaponType.Staffs;
            double damage = 4;
            Slot slot = Slot.Weapon;

            Weapon newWeapon = new Weapon(weaponName, requiredLevel, type, damage, slot);

            Dictionary<Slot, Item> addWeapon = new Dictionary<Slot, Item>();
            addWeapon.Add(slot, newWeapon);
            heroMage.Equip(newWeapon);
            var expected = addWeapon[Slot.Weapon];

            //Act
            var actual = heroMage.Equipments[Slot.Weapon];

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipWeapon_InvalidLevel3_ShouldReturnAppropriateException()
        {
            //Arrange
            string name = "Thea";

            Hero heroMage = new Mage(name);
            string weaponName = "staff";
            int requiredLevel = 3;
            WeaponType type = WeaponType.Staffs;
            double damage = 4;
            Slot slot = Slot.Weapon;

            Weapon newWeapon = new Weapon(weaponName, requiredLevel, type, damage, slot);

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => heroMage.Equip(newWeapon));

        }

        [Fact]
        public void EquipWeapon_InvalidWeaponTypeDagger_ShouldReturnAppropriateException()
        {
            //Arrange
            string name = "Thea";

            Hero heroMage = new Mage(name);
            string weaponName = "dagger";
            int requiredLevel = 1;
            WeaponType type = WeaponType.Daggers;
            double damage = 4;
            Slot slot = Slot.Weapon;

            Weapon newWeapon = new Weapon(weaponName, requiredLevel, type, damage, slot);

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => heroMage.Equip(newWeapon));
        }

        //EquipArmor
        [Fact]
        public void EquipArmor_Plate_ShouldBeEquipped()
        {
            //Arrange
            string name = "Thea";
            Hero heroWarrior = new Warrior(name);
            string armorName = "shield";
            int requiredLevel = 1;
            ArmorType type = ArmorType.Plate;
            Slot slot = Slot.Body;
            HeroAttribute armorAttribute = new HeroAttribute(1, 6, 2);

            Armor newArmor = new Armor(armorName, requiredLevel, type, slot, armorAttribute);

            Dictionary<Slot, Item> addArmor = new Dictionary<Slot, Item>();
            addArmor.Add(slot, newArmor);
            heroWarrior.Equip(newArmor, slot);

            var expected = addArmor[slot];

            //Act
            var actual = heroWarrior.Equipments[slot];

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_InvalidLevel3_ShouldReturnAppropriateException()
        {
            //Arrange
            string name = "Bob";
            Hero heroRanger = new Ranger(name);
            string armorName = "boots";
            int requiredLevel = 3;
            ArmorType type = ArmorType.Leather;
            Slot slot = Slot.Head;
            HeroAttribute armorAttribute = new HeroAttribute(2, 5, 3);

            Armor newArmor = new Armor(armorName, requiredLevel, type, slot, armorAttribute);

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => heroRanger.Equip(newArmor, slot));
        }

        [Fact]
        public void EquipArmor_InvalidArmorTypeCloth_ShouldReturnAppropriateException()
        {
            //Arrange
            string name = "Bob";
            Hero heroRanger = new Ranger(name);
            string armorName = "cape";
            int requiredLevel = 1;
            ArmorType type = ArmorType.Cloth;
            Slot slot = Slot.Body;
            HeroAttribute armorAttribute = new HeroAttribute(2, 5, 3);

            Armor newArmor = new Armor(armorName, requiredLevel, type, slot, armorAttribute);

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => heroRanger.Equip(newArmor, slot));
        }

        //Total Attributes should be calculated correctly
        [Fact]
        public void CalculateTotalAttributes_NoEquipment_ShouldReturnCorrectAttribute()
        {
            //Arrange
            string name = "Thea";
            Hero heroMage = new Mage(name);

            HeroAttribute total = heroMage.LevelAttributes;
            total.Strength = 1;
            total.Dexterity = 1;
            total.Intelligence = 8;

            HeroAttribute expected = total;

            //Act
            var actual = heroMage.TotalAttributes();

            //Assert
            Assert.Equal(expected, actual);


        }

        [Fact]
        public void CalculateTotalAttributes_WithOneArmor_ShouldReturnCorrectAttribute()
        {
            //Arrange
            string name = "Thea";
            Hero heroRanger = new Ranger(name);

            string armorName = "boots";
            int requiredLevel = 1;
            ArmorType type = ArmorType.Leather;
            Slot slot = Slot.Legs;

            int strength = 2;
            int dexterity = 5;
            int intelligence = 3;
            HeroAttribute armorAttribute = new HeroAttribute(strength, dexterity, intelligence);

            Armor armor = new Armor(armorName, requiredLevel, type, slot, armorAttribute);
            heroRanger.Equip(armor, slot);


            HeroAttribute total = heroRanger.LevelAttributes;
            total.Strength = strength + total.Strength;
            total.Dexterity = dexterity + total.Dexterity;
            total.Intelligence = intelligence + total.Intelligence;

            var expected = total;

            //Act
            var actual = heroRanger.TotalAttributes();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateTotalAttributes_WithTwoArmors_ShouldReturnCorrectAttribute()
        {
            //Arrange
            string name = "Thea";
            Hero heroRanger = new Ranger(name);

            string armorNameBoot = "boots";
            int requiredLevelBoot = 1;
            ArmorType typeBoot = ArmorType.Leather;
            Slot slotBoot = Slot.Head;

            int strengthBoot = 2;
            int dexterityBoot = 5;
            int intelligenceBoot = 3;
            HeroAttribute armorAttributeBoot = new HeroAttribute(strengthBoot, dexterityBoot, intelligenceBoot);

            Armor armorBoot = new Armor(armorNameBoot, requiredLevelBoot, typeBoot, slotBoot, armorAttributeBoot);
            heroRanger.Equip(armorBoot, slotBoot);

            string armorNameHood = "hood";
            int requiredLevelHood = 1;
            ArmorType typeHood = ArmorType.Leather;
            Slot slotHood = Slot.Head;

            int strengthHood = 4;
            int dexterityHood = 1;
            int intelligenceHood = 2;
            HeroAttribute armorAttributeHood = new HeroAttribute(strengthHood, dexterityHood, intelligenceHood);

            Armor armorHood = new Armor(armorNameHood, requiredLevelHood, typeHood, slotHood, armorAttributeHood);
            heroRanger.Equip(armorHood, slotHood);


            HeroAttribute total = heroRanger.LevelAttributes;
            total.Strength = strengthHood + strengthBoot + total.Strength;
            total.Dexterity = dexterityHood + dexterityHood + total.Dexterity;
            total.Intelligence = intelligenceBoot + intelligenceHood + total.Intelligence;

            var expected = total;

            //Act
            var actual = heroRanger.TotalAttributes();

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CalculateTotalAttributes_WithReplacedArmor_ShouldReturnCorrectAttribute()
        {
            //Arrange
            string name = "Thea";
            Hero heroRanger = new Ranger(name);

            string armorName = "boots";
            int requiredLevel = 1;
            ArmorType type = ArmorType.Leather;
            Slot slot = Slot.Legs;

            int strength = 2;
            int dexterity = 5;
            int intelligence = 3;
            HeroAttribute armorAttribute = new HeroAttribute(strength, dexterity, intelligence);

            Armor armor = new Armor(armorName, requiredLevel, type, slot, armorAttribute);
            heroRanger.Equip(armor, slot);

            string newArmorName = "leggings";
            int newRequiredLevel = 1;
            ArmorType newType = ArmorType.Leather;
            Slot newSlot = Slot.Legs;

            int newStrength = 4;
            int newDexterity = 1;
            int newIntelligence = 2;
            HeroAttribute newArmorAttribute = new HeroAttribute(newStrength, newDexterity, newIntelligence);

            Armor newArmor = new Armor(newArmorName, newRequiredLevel, newType, newSlot, newArmorAttribute);
            heroRanger.Equip(newArmor, newSlot);


            HeroAttribute total = heroRanger.LevelAttributes;
            total.Strength = newStrength + total.Strength;
            total.Dexterity = newDexterity + total.Dexterity;
            total.Intelligence = newIntelligence + total.Intelligence;

            var expected = total;

            //Act
            var actual = heroRanger.TotalAttributes();

            //Assert
            Assert.Equal(expected, actual);
        }

        //CalculateDamage
        [Fact]
        public void CalculateDamage_NoWeaponEquipped_ShouldReturnCorrectCalculation()
        {
            //Arrange
            string name = "Thea";
            Hero mageHero = new Mage(name);

            var damageAttribute = mageHero.LevelAttributes;
            var expected = 1 * (1 + damageAttribute.Intelligence / 100);

            //Act
            var actual = mageHero.CalculateDamage();

            //Assert
            Assert.Equal(expected, actual);


        }

        [Fact]
        public void CalculateDamage_WeaponEquipped_ShouldReturnCorrectCalculation()
        {
            //Arrange
            string name = "Thea";
            Hero mageHero = new Mage(name);

            string weaponName = "wand";
            int requiredLevel = 1;
            WeaponType type = WeaponType.Wands;
            double damage = 5;
            Slot slot = Slot.Weapon;

            Weapon weapon = new Weapon(weaponName, requiredLevel, type, damage, slot);
            mageHero.Equip(weapon);

            var damageAttribute = mageHero.LevelAttributes;
            var expected = damage * (1 + (double)damageAttribute.Intelligence / 100);

            //Act
            var actual = mageHero.CalculateDamage();

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CalculateDamage_ReplacedWeaponEquipped_ShouldReturnCorrectCalculation()
        {
            //Arrange
            string name = "Thea";
            Hero mageHero = new Mage(name);

            string weaponName = "wand";
            int requiredLevel = 1;
            WeaponType type = WeaponType.Wands;
            double damage = 5;
            Slot slot = Slot.Weapon;

            Weapon weapon = new Weapon(weaponName, requiredLevel, type, damage, slot);
            mageHero.Equip(weapon);

            string newWeaponName = "straff";
            int newRequiredLevel = 1;
            WeaponType newType = WeaponType.Staffs;
            double newDamage = 4;
            Slot newSlot = Slot.Weapon;

            Weapon newWeapon = new Weapon(newWeaponName, newRequiredLevel, newType, newDamage, newSlot);
            mageHero.Equip(newWeapon);

            var damageAttribute = mageHero.LevelAttributes;
            var expected = newDamage * (1 + (double)damageAttribute.Intelligence / 100);

            //Act
            var actual = mageHero.CalculateDamage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_WeaponAndArmorEquipped_ShouldReturnCorrectCalculation()
        {
            //Arrange
            string name = "Bob";
            Hero warriorHero = new Warrior(name);

            string weaponName = "sword";
            int weaponRequiredLevel = 1;
            WeaponType weaponType = WeaponType.Swords;
            double damage = 6;
            Slot weaponSlot = Slot.Weapon;

            Weapon weapon = new Weapon(weaponName, weaponRequiredLevel, weaponType, damage, weaponSlot);
            warriorHero.Equip(weapon);

            string armorName = "shield";
            int armorRequiredLevel = 1;
            ArmorType armorType = ArmorType.Plate;
            Slot armorSlot = Slot.Body;
            HeroAttribute armorAttribute = new HeroAttribute(3, 1, 6);

            Armor armor = new Armor(armorName, armorRequiredLevel, armorType, armorSlot, armorAttribute);
            warriorHero.Equip(armor, armorSlot);

            warriorHero.TotalAttributes();

            var damageAttribute = warriorHero.LevelAttributes;
            var expected = damage * (1 + (double)damageAttribute.Strength / 100);

            //Act
            var actual = warriorHero.CalculateDamage();

            //Assert
            Assert.Equal(expected, actual);


        }


    }
}