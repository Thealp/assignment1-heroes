# Assignment1-Heroes

## Table of Contents
- Background
- Install
- Author

## Background
Rpg-Heroes is a .NET console application, which was made as an assignment assigned by Noroff. It contains hero-classes (mage, ranger, rogue, warrior), which have attributes that increases when the hero levels up. The project also contains items (armor and weapons) that the heroes can equip. When equipping a new item, the heroes power will increase, causing it to deal more damage and survive longer. A hero has certain weapons and armors they can equip. 
The project also contains custom exceptions, and unit testing of the main functionality. 

## Install
Clone repositoy in Visual Studio: git clone https://gitlab.com/Thealp/assignment1-heroes.git 

## Author
Gitlab username: Thealp

Gitlab: [Gitlab-Link](https://gitlab.com/Thealp)




