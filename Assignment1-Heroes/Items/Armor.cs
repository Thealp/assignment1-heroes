﻿
using Assignment1_Heroes.Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Heroes.Items
{
    public class Armor : Item
    {
       
        public HeroAttribute ArmorAttribute { get; set; } //provide bonuses to a heroes attributes when equipped

        public ArmorType ArmorType { get; set; }
        public Armor(string name, int requiredLevel, ArmorType armorType, Slot slot, HeroAttribute armorAttribute) : base(name, requiredLevel, slot)
        {
            ArmorAttribute = armorAttribute;
            ArmorType = armorType;

        }
    }
}
