﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Heroes.Items
{
    public class Weapon : Item
    {
        public double WeaponDamage { get; set; }   
      
        public WeaponType WeaponType { get; set; }

        public Weapon(string name, int requiredLevel, WeaponType weaponType, double weaponDamage,  Slot slot) : base(name, requiredLevel, slot)
        {
            WeaponDamage = weaponDamage;
            WeaponType = weaponType;
        }
    }
}
