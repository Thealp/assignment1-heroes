﻿
using Assignment1_Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Heroes.Heroes
{
    public class Rogue : Hero
    {
        public Rogue(string name) : base(name)
        {
            Level = 1;
            LevelAttributes = new HeroAttribute(2, 6, 1);
            ValidWeaponTypes = new List<WeaponType>() { WeaponType.Daggers, WeaponType.Swords };
            ValidArmorTypes = new List<ArmorType>() { ArmorType.Leather, ArmorType.Mail };
        }

        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes.Increase(1, 4, 1);
        }

        public override void Equip(Weapon weapon)
        {

            base.Equip(weapon);
            Equipments[Slot.Weapon] = weapon;
        }

        public override void Equip(Armor armor, Slot slot)
        {
            base.Equip(armor, slot);
            Equipments[slot] = armor;
        }

        public override double CalculateDamage()
        {
            if (Equipments[Slot.Weapon] == null)
            {
                return 1 * (1 + LevelAttributes.Dexterity / 100);
            }
            else
            {
                Weapon? weapon = (Weapon?)Equipments[Slot.Weapon];
                return Math.Round(weapon.WeaponDamage * (1 + (double)LevelAttributes.Dexterity / 100), 2);
            }

        }

        public override string DisplayHero()
        {
            return base.DisplayHero();
        }
        


    }
}
