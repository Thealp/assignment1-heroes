﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Heroes.Heroes
{
    public class HeroAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }  
        public int Intelligence { get; set; }

        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }


        //Method to add two instances together and return the sum or increase the one instance by a specified amount
        public void Increase(int strength, int dexterity, int intelligence)
        {
            Strength += strength;
            Dexterity += dexterity;
            Intelligence += intelligence;
        }

        public void Increase(HeroAttribute attribute)
        {
            Strength += attribute.Strength;
            Dexterity += attribute.Dexterity;
            Intelligence += attribute.Intelligence;
        }
    }   
}
