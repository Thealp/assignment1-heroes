﻿
using Assignment1_Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Heroes.Heroes
{
    public class Mage: Hero
    {
        
        public Mage(string name) : base(name)
        {
            
            Level = 1;
            LevelAttributes = new HeroAttribute(1, 1, 8);

            ValidWeaponTypes = new List<WeaponType>() { WeaponType.Staffs, WeaponType.Wands };
            ValidArmorTypes = new List<ArmorType>() { ArmorType.Cloth };
            
        }
        
        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes?.Increase(1, 1, 5);
        }

        public override void Equip(Weapon weapon)
        {
           
            base.Equip(weapon);
            Equipments[Slot.Weapon] = weapon;
          
        }

        public override void Equip(Armor armor, Slot slot)
        {
            base.Equip(armor, slot);
            Equipments[slot] = armor;
 
        }

 
        public override double CalculateDamage()
        { 
            if (Equipments[Slot.Weapon] == null)
            {
                return 1 * (1 + LevelAttributes.Intelligence/100);
            }
            else
            {
                Weapon? weapon = (Weapon?)Equipments[Slot.Weapon];
                return Math.Round(weapon.WeaponDamage * (1 + (double)LevelAttributes.Intelligence / 100), 2);
            }
           
        }

        public override string DisplayHero()
        {
            return base.DisplayHero();
        }


    }
}
