﻿
using Assignment1_Heroes.Exceptions;
using Assignment1_Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Heroes.Heroes
{
    public abstract class Hero
    {
        public string Name { get; set; } 
        public int Level { get; set; }
        public HeroAttribute? LevelAttributes { get; set; }
        public Dictionary <Slot, Item?> Equipments { get; set; }   
        public List <WeaponType>? ValidWeaponTypes { get; set; } 
        public List <ArmorType>?ValidArmorTypes { get; set; } 

        //Methods
        public Hero(string name)
        {
            Name = name;

            Equipments = new Dictionary<Slot, Item?> ();
            Equipments.Add(Slot.Weapon, null);
            Equipments.Add(Slot.Head, null);
            Equipments.Add(Slot.Body, null);
            Equipments.Add(Slot.Legs, null);
            
        }

        //LevelUp
        public virtual void LevelUp()
        {
            Level++;
            Console.WriteLine("LevelUp");
        }

        //Equip
        public virtual void Equip(Weapon weapon)
        {
        
            if (weapon.RequiredLevel > Level)
            {
                throw new InvalidWeaponException("Weapon has a higher required level"); 
            }
           
            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException("Weapontype can not be equipped for your class");
            }
            
            Console.WriteLine("Equipped weapon: " + weapon.Name);
        }
        

        public virtual void Equip(Armor armor, Slot slot)
        {
         
            if (armor.RequiredLevel > Level)
            {
                throw new InvalidArmorException("Armor has a higher required level");
                
            }

            if (!ValidArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException("Armortype can not be equipped for your class");
            }

           
            Console.WriteLine("Equipped armor: " + armor.Name);
           
        }


        //TotalAttributes
        public virtual HeroAttribute? TotalAttributes()
        {
            HeroAttribute? totalAttributes = LevelAttributes;

            foreach(var item in Equipments) 
             {

                if (item.Key != Slot.Weapon && item.Value != null )
                 {

                    totalAttributes.Strength += ((Armor)item.Value).ArmorAttribute.Strength;
                    totalAttributes.Dexterity += ((Armor)item.Value).ArmorAttribute.Dexterity;
                    totalAttributes.Intelligence += ((Armor)item.Value).ArmorAttribute.Intelligence;

                }      
             }
            return totalAttributes;       

        }

        //Damage
        public abstract double CalculateDamage();
        
        public virtual string DisplayHero()
        {
            StringBuilder sb = new StringBuilder();
            HeroAttribute? total = TotalAttributes();

            sb.Append("Name: " +Name);
            sb.Append(", Level: " +Level);
            sb.Append(", Strength: "+ total?.Strength); 
            sb.Append(", Dexterity: "+ total?.Dexterity);   
            sb.Append(", Intelligence: " +total?.Intelligence);
            sb.Append(", Herodamage: " + CalculateDamage());

            return sb.ToString();
        }
        
    }
}